%define offset 8

section .text

extern string_equals
global find_word

find_word:
        ; rdi - указатель на нуль-терминированную строку
        ; rsi - указатель на начало словаря
        ; ret:
        ;    rax = 0 если ключ не найден
        ;    иначе адрес начала вхождения в словарь в rax
    
        push rdi
        push rsi

        add rsi, offset
        call string_equals

        pop rsi
        pop rdi

        cmp rax, 1
        je .success

        mov rsi, [rsi] ; next key
        cmp rsi, 0
        jne find_word

        ; If key not found
        xor rax, rax
        ret
.success:
        mov rax, rsi
        ret

