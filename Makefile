ASM=nasm
ASMFLAGS=-f elf64
LD=ld
RM=rm

dictionary: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	$(RM) *.o main

main.asm: lib.inc words.inc colon.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
