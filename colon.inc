%define pointer 0

%macro colon 2
        %%cur_ptr:
        dq pointer
        db %1, 0
        %define pointer %%cur_ptr

        %2:
%endmacro
