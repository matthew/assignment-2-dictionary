section .rodata

long_word_mes: db 'Word length should be less than 256', 10, 0
not_found_mes: db 'There is no such word in dictionary', 10, 0

section .data

%include "lib.inc"
%include "words.inc"
extern find_word

%define offset 8
%define string_max_length 256

section .text
global _start 


not_found_error:
        mov rdi, not_found_mes
        jmp print_error
long_word_error:
        mov rdi, long_word_mes
print_error:
        call print_string

        xor rdi, rdi
        jmp exit

read_key:
        ; no args
        ; ret: 
        ;    rax - указатель на буфер
        ;    rdx - длина ключа

        mov rsi, string_max_length
        mov rdi, rsp
        sub rdi, rsi

        call read_line

        cmp rax, 0
        je long_word_error
        ret

find_key:
        ; rdi - указатель на нуль-терминированную строку
        ; ret:
        ;    rax - адрес ключа
        mov rsi, pointer
        call find_word
 
        cmp rax, 0
        je not_found_error
        ret

_start:
        call read_key

        push rdx      ; save key lenght

        mov rdi, rax
        call find_key

        pop rdx

        add rax, offset
        add rax, rdx     ; add key lenght
        inc rax          ; zero-terminator
        mov rdi, rax     ; put pointer on data in rdi

        call print_string
        call print_newline

        xor rdi, rdi
        jmp exit
